# README #

This is a collection of coding standards for the IntelliJ Ideal IDE (or derivatives
such as Android Studio) as used by me, Steve Dobson.

### How do I get set up? ###

You will need to import the appropriate coding standards configuration file into the IDE:
Navigation to Settings -> Editor -> Inspections and then selected "Import" from the "Manage" pull down list. 